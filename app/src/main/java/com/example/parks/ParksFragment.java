package com.example.parks;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.parks.Adapater.ParkRecycleViewAdapter;
import com.example.parks.data.AsyncResponse;
import com.example.parks.data.Repository;
import com.example.parks.model.Park;

import java.util.List;


public class ParksFragment extends Fragment {

public RecyclerView recyclerView;
public ParkRecycleViewAdapter parkRecycleViewAdapter;
public List<Park> parkList;


 public ParksFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ParksFragment newInstance() {
        ParksFragment fragment = new ParksFragment();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Repository.getparks(new AsyncResponse() {
            @Override
            public void processpark(List<Park> parks) {
                parkRecycleViewAdapter= new ParkRecycleViewAdapter(parks);
                recyclerView.setAdapter(parkRecycleViewAdapter);
            }
        });


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_parks, container, false);
        recyclerView= view.findViewById(R.id.parks_recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }
}