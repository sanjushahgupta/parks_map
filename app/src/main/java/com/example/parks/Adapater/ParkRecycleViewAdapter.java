package com.example.parks.Adapater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.parks.R;
import com.example.parks.model.Park;
import com.squareup.picasso.Picasso;

import java.util.List;



public class ParkRecycleViewAdapter extends RecyclerView.Adapter<ParkRecycleViewAdapter.ViewHolder>
{
    private final List<Park> parkList;

    public ParkRecycleViewAdapter(List<Park> parkList) {
        this.parkList = parkList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.park_row, parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Park park=parkList.get(position);
       holder.parkName.setText(park.getName());
      //  holder.parkType.setText(park.getDesignation());
        holder.parkState.setText(park.getDescription());

        if(park.getImages().size()>0)
        {
            Picasso.get().load(park.getImages().get(0).getUrl()).
                    placeholder(android.R.drawable.stat_sys_download).error(android.R.drawable.stat_notify_error).
                    resize(100,200).centerCrop().into(holder.parkImage);
        }


    }

    @Override
    public int getItemCount()
    {
        return parkList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView parkImage;
        public TextView parkName;
        public TextView parkType;
        public TextView parkState;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            parkImage=itemView.findViewById(R.id.row_park_imageview);
            parkName=itemView.findViewById(R.id.row_park_name_textview);
            //parkType=itemView.findViewById(R.id.row_park_type_textview);
            parkState=itemView.findViewById(R.id.row_park_state_textview);
        }
    }

}
